<?php

namespace App\Http\Controllers;

use App\Komentar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Komentar::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Daftar komentar berhasil ditampilkan',
            'data'    => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'isi_komentar' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $post = Komentar::create([
            'isi_komentar' =>  $request->isi_komentar,
        ]);

        if($post){
            return response()->json([
                'success'   => true,
                'message'   => 'Komentarmu berhasil dibuat',
                'data'      =>  $post
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Komentar gagal dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Komentar::find($id);

        if($post)
        {
            return response()->json([
                'success' => true,
                'message' => 'Komentar berhasil ditampilkan',
                'data'    => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'isi_komentar' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Komentar::find($id);

        if($post)
        {
            $post->update([
                'isi_komentar' => $request->isi_komentar,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Komentar dengan id : ' . $post->id . '  berhasil diupdate',
                'data' =>    $post
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Komentar dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Komentar::find($id);

        if ($post) 
        {
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Komentar berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Komentar dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
