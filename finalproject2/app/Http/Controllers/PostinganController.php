<?php

namespace App\Http\Controllers;

use App\Postingan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostinganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Postingan::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar ulasan berhasil ditampilkan',
            'data'    => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'isi' => 'required',
            'gambar' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $post = Postingan::create([
            'isi' =>  $request->isi,
            'gambar' => $request->gambar,
        ]);

        if($post){
            return response()->json([
                'success'   => true,
                'message'   => 'Ulasanmu berhasil dibuat',
                'data'      =>  $post
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Ulasan gagal dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Postingan::find($id);

        if($post)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data ulasan berhasil ditampilkan',
                'data'    => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'isi' => 'required',
            'gambar' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Postingan::find($id);

        if($post)
        {
            $post->update([
                'isi' => $request->isi,
                'gambar' => $request->gambar,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan id : ' . $post->id . '  berhasil diupdate',
                'data' =>    $post
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Postingan::find($id);

        if ($post) 
        {
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
