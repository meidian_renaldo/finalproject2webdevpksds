<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $fillable = ['id', 'isi_komentar', 'User_id', 'Postingan_id'];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function postingan()
    {
        return $this->belongsTo('App\Postingan');
    }
}
