<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentars', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('isi_komentar');
            $table->uuid('User_id')->nullable();
            $table->uuid('Postingan_id')->nullable();
            $table->foreign('User_id')->references('id')->on('users');
            $table->foreign('Postingan_id')->references('id')->on('postingans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentars');
    }
}
